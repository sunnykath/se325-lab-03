package se325.lab03.parolee.domain;

/**
 * Enumerated type to represent gender.
 */
public enum Gender {
    MALE, FEMALE;
}
